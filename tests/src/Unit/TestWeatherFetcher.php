<?php

namespace Drupal\Tests\weather\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Tests\UnitTestCase;
use Drupal\weather\WeatherFetcher;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class TestWeatherFetcher extends UnitTestCase
{


  /**
   * Tests the getWeather method.
   */
  public function testGetWeather()
  {
    // Mock the response:
    $response_example = file_get_contents(__DIR__ . '/../../fixtures/api_response.json');
    $streamMock = $this->getMockBuilder(StreamInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $streamMock->expects($this->any())->method('getContents')->willReturn($response_example);
    $responseMock = $this->getMockBuilder(ResponseInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $responseMock->expects($this->any())->method('getBody')->willReturn($streamMock);
    $httpClient = $this->getMockBuilder(Client::class)
      ->disableOriginalConstructor()
      ->getMock();

    // Mock the config:
    $conf = $this->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $confValues = [
      ['city', 'beirut'],
      ['country', 'lb'],
      ['api_key', 'be306854c8951ab7c36943603da82b4d'],
      ['api_endpoint', 'https://api.openweathermap.org/data/2.5/weather']
    ];
    $conf->expects($this->any())->method('get')->will($this->returnValueMap($confValues));
    // Mock the config factory:
    $configFactory = $this->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $configFactory->expects($this->any())->method('get')->willReturn($conf);

    // Mock the http client:
    $expected_url = "https://api.openweathermap.org/data/2.5/weather?q=beirut,lb&appid=be306854c8951ab7c36943603da82b4d";
    $httpClient->expects($this->any())->method('request')->with('GET', $expected_url)->willReturn($responseMock);
    $weatherFetcher = new WeatherFetcher($configFactory, $httpClient);

    // Assert the result:
    $result = $weatherFetcher->getWeather();
    $this->assertEquals(json_decode($response_example, TRUE), $result);
  }
}
