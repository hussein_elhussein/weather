<?php

namespace Drupal\Tests\weather\Functional;

use Drupal\Tests\BrowserTestBase;

abstract class BaseFunctionalTest extends BrowserTestBase
{

  /**
   * A user with permission to administer feeds and create content.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  protected $defaultTheme = "stark";

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'weather',
  ];

  protected function setUp()
  {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer weather',
      'access content',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Submits the configuration form and tests it.
   */
  protected function submitConfigForm(){
    // Submit the form:
    $this->drupalGet('/admin/config/services/weather');
    $data = [
      'weather_city' => "Beirut",
      'weather_country' => 'lb',
      'weather_api_endpoint' => 'https://api.openweathermap.org/data/2.5/weather',
      'weather_api_key' => 'be306854c8951ab7c36943603da82b4d',
    ];
    $this->submitForm($data, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // assert the form values been saved:
    $factory = $this->container->get('config.factory');
    $country = $factory->get('weather.settings')->get('country');
    $this->assertEquals('lb', $country);
  }
}
