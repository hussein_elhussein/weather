<?php

namespace Drupal\Tests\weather\Functional;


use Drupal\Core\Config\ConfigFactoryInterface;

class WeatherControllerTest extends BaseFunctionalTest
{

  /**
   * Tests the functionality of the weather page.
   */
  public function testWeather(){
    $this->submitConfigForm();
    $this->drupalGet('/weather');

    // assert that it displays the weather info:
    $this->assertSession()->pageTextContains('Condition:');
  }
}
