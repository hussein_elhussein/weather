<?php

namespace Drupal\weather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures weather settings for this site.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {


  /**
   * Constructs a \Drupal\aggregator\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $string_translation) {
    parent::__construct($config_factory);
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['weather.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('weather.settings');

    // Global aggregator settings.
    $form['weather_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default City'),
      '#size' => 80,
      '#maxlength' => 255,
      '#default_value' => $config->get('city'),
      '#description' => $this->t('Enter a default city.'),
    ];

    // Global aggregator settings.
    $form['weather_country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Country'),
      '#size' => 80,
      '#maxlength' => 255,
      '#default_value' => $config->get('country'),
      '#description' => $this->t('Enter a default country.'),
    ];

    $form['weather_api_endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('API Endpoint'),
      '#size' => 80,
      '#maxlength' => 255,
      '#default_value' => $config->get('api_endpoint'),
      '#description' => $this->t('The API Endpoint of the weather service.'),
    ];

    $form['weather_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#size' => 80,
      '#maxlength' => 255,
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('API Key for the weather service.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('weather.settings');
    $config->set('city', $form_state->getValue('weather_city'));
    $config->set('country', $form_state->getValue('weather_country'));
    $config->set('api_endpoint', $form_state->getValue('weather_api_endpoint'));
    $config->set('api_key', $form_state->getValue('weather_api_key'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
