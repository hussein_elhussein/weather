<?php

namespace Drupal\weather\Controller;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\weather\WeatherFetcher;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Query the weather information.
 * @package Drupal\weather\Controller
 */
class WeatherController extends ControllerBase {

  protected $weatherFetcher;


  /**
   * WeatherController constructor.
   *
   * @param WeatherFetcher $weatherFetcher
   *   The HTTP Client.
   */
  public function __construct(WeatherFetcher $weatherFetcher)
  {
    $this->weatherFetcher = $weatherFetcher;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('weather.fetcher'),
    );
  }

  /**
   * Gets the weather information.
   *
   * @param string|null $city
   * @param string|null $country
   * @return array
   */
  public function weather($city = null, $country = null){
    $result = null;
    $err = null;
    try {
      $result = $this->weatherFetcher->getWeather($city, $country);
    } catch (GuzzleException $e) {
      $err = [
        'code' => $e->getCode(),
        'message' => $e->getMessage(),
      ];
    }

    if (!$result) {
      $res = [
        '#theme' => 'weather_info',
        '#error' => $this->t('Could not connect to the weather API endpoint'),
      ];
      if ($err) {
        $res['#error'] = $this->t('Could not connect to the weather API endpoint: @code : @message',
          ['@code' => $err['code'], '@message' => $err['message']]);
      }
      return $res;
    }

    $weather_info = [
      'state' => $result['weather'][0]['description'],
      'temp' => $result['main']['temp'],
      'temp_min' => $result['main']['temp_min'],
      'temp_max' => $result['main']['temp_max'],
      'wind' => $result['wind']['speed'],
    ];

    return [
      '#theme' => 'weather_info',
      '#info' => $weather_info,
    ];
  }
}
