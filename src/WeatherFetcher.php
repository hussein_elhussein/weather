<?php

namespace Drupal\weather;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Communicates with the api to get the weather info.
 *
 * @package Drupal\weather
 */
class WeatherFetcher
{

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var ClientInterface
   */
  protected $httpClient;


  /**
   * WeatherController constructor.
   *
   * @param ClientInterface $http_client
   *   The HTTP Client.
   * @param ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $http_client)
  {
    $this->configFactory = $configFactory;
    $this->httpClient = $http_client;
  }

  /**
   * Fetches the weather information.
   *
   * @param string|null $city
   * @param string|null $country
   *
   * @return array|null
   * @throws GuzzleException
   */
  public function getWeather($city = null, $country = null){
    $config = $this->configFactory->get('weather.settings');
    $api_endpoint = $config->get('api_endpoint');
    $api_key = $config->get('api_key');
    if (!$city) {
      $city = $config->get('city');
    }
    if (!$country) {
      $country = $config->get('country');
    }
    $url = "${api_endpoint}?q=${city},${country}&appid=${api_key}";
    $result = $this->httpClient->request('GET', $url);
    $result = json_decode($result->getBody()->getContents(), TRUE);
    if ($result) {
      return $result;
    }
    return null;
  }
}
